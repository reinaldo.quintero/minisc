from django.db import models

# Create your models here.

class App(models.Model):
    items = models.CharField(max_length=100)
    address = models.TextField()
    carrier = models.CharField(max_length=25)


class Parcel(models.Model):
    items = models.CharField(max_length=100)
    address = models.TextField()
    carrier = models.CharField(max_length=25)
