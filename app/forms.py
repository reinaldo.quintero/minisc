from django import forms
from .models import App

class ParcelForm(forms.ModelForm):
    class Meta:
        model = App

        fields = ['items', 'address', 'carrier']

