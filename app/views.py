from django.shortcuts import render
from app.models import Parcel
from .forms import ParcelForm
from django.shortcuts import redirect


# Create your views here.


def all_parcels(request):
    parcel = Parcel.objects.all()
    return render(request, 'app/all_parcels.html',
                  {'parcel': parcel})

def parcel_details(request, pk):
    parcel = Parcel.objects.get(pk=pk)
    return render(request, 'app/detail.html',
                  {'parcel': parcel})

def add_parcel(request):
    if request.method == "POST":
        form = ParcelForm(request.POST)
        if form.is_valid():
            parcel = form.save()
            return redirect('app:parcel_details', pk=parcel.pk)
    else:
        form = ParcelForm()
    return render(request, 'app/add_parcel.html', {'form': form})