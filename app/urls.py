from django.contrib import admin
from django.urls import path
from app import views

app_name = 'app'

urlpatterns = [
    path('', views.all_parcels, name='all_parcels'),
    path('<int:pk>', views.parcel_details, name='parcel_details'),
    path('add-parcel', views.add_parcel, name='add_parcel')
]