from django.apps import apps as global_apps
from django.db import migrations

def forwards(apps, schema_editor):
    App = apps.get_model('app', 'App')
    Parcel = apps.get_model('app', 'Parcel')
    Parcel.objects.bulk_create(
        Parcel(items=app_object.items,carrier=app_object.carrier,address=app_object.address)
        for app_object in App.objects.all()
    )

class Migration(migrations.Migration):
    operations = [
        migrations.RunPython(forwards, migrations.RunPython.noop),
    ]
    dependencies = [
        ('app', '0002_parcel'),
    ]
